/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jstoccupation;

import java.util.Random;

/**
 *
 * @author ilhammaziz
 */
public final class NeuralNetwork {

    public static Random getRandom() {
        return random;
    }

    public static void setRandom(Random aRandom) {
        random = aRandom;
    }
    private int inputLayerSize;
    private int outputLayerSize;
    private int hiddenLayerSize;
    
    private Matrix HL2Matrix;
    private Matrix rawHL2Matrix;
    private Matrix FFWItoHMatrix;
    private Matrix FFWHtoIMatrix;
    private Matrix FBWItoHMatrix;
    private Matrix FBWHtoIMatrix;
    /*private double[] HL2Matrix;
    private double[][] FFWItoHMatrix;
    private double[][] FFWHtoIMatrix;
    private double[][] FBWItoHMatrix;
    private double[][] FBWHtoIMatrix;*/
    private Matrix FFBias;
    private double FFBiasOutput;
    private Matrix FBBias;
    private double FBBiasOutput;
    private double output;
    
    private static Random random=new Random();
    
    public NeuralNetwork(int il, int hl,int ol){
        this.inputLayerSize=il;
        this.hiddenLayerSize=hl;
        this.outputLayerSize=ol;
        
        FFWItoHMatrix=new Matrix(il,hl);
        FFWHtoIMatrix=new Matrix(hl,1);
        FFBias=new Matrix(1, hl);
        
        FBWItoHMatrix = new Matrix(il, hl);
        FBWHtoIMatrix = new Matrix(hl, 1);
        FBBias=new Matrix(hl, 1);
        
        HL2Matrix=new Matrix(1, hl);
        rawHL2Matrix=new Matrix(1, hl);
        output=0;
    }
    
    public void printMatrix(double[] mt){
        for(int i=0;i<mt.length;i++){
                System.out.printf(mt[i]+"\t");
        }
        System.out.println("");
    }
    
    public static double randomize(double nMin, double nMax) {
        double range = nMax - nMin;
        double scale = getRandom().nextDouble() * range;
        return (scale + nMin);
    }
    
    public void feedBack(Matrix inputSet, double err,double alpha){
        for(int i=0;i<inputLayerSize;i++){
            for(int j=0;j<hiddenLayerSize;j++){
                FBWItoHMatrix.matrix[i][j]=err*output*(1-output)*FFWHtoIMatrix.get(j, 0)*HL2Matrix.get(0, j)*(1-HL2Matrix.get(0, j))*inputSet.get(0, i)*alpha;
                //FBBias.set(err*output*(1-output)*FBWHtoIMatrix.get(j, 0)*HL2Matrix.get(0, j)*(1-HL2Matrix.get(0, j))*alpha, 0, j);
            }
        }
        
        for(int i=0;i<hiddenLayerSize;i++){
            FBWHtoIMatrix.set(err*output*(1-output)*HL2Matrix.get(0, i)*alpha, i, 0);
        }
        
        FBBiasOutput=output*err;
    }
    
    public double feedForward(Matrix inputSet){
        double buff3;
        
        Matrix buff2=dotProduct(inputSet, getFFWItoHMatrix());
        
        for(int i=0;i<getHiddenLayerSize();i++){
            //this.getHL2Matrix().set(sigmoid(buff2.get(0, i) + getFFBias().get(0, i)),0,i);
            //System.out.println(buff2.get(0, i));
            this.getRawHL2Matrix().set(buff2.get(0, i),0,i);
            this.getHL2Matrix().set(sigmoid(buff2.get(0, i)),0,i);
        }
        
        //System.out.println("===============");
        //getHL2Matrix().printMatrix();
        //System.out.println("===============");
        
        Matrix matrixOutput=dotProduct(getHL2Matrix(),getFFWHtoIMatrix());
        //setOutput(sigmoid(matrixOutput.get(0, 0))+getFFBiasOutput());
        setOutput(sigmoid(matrixOutput.get(0, 0)));
        
        //return getOutput();
        return matrixOutput.get(0, 0);
    }
    
    public Matrix dotProduct(Matrix m1,Matrix m2){
        Matrix result=new Matrix(m1.rowSize, m2.colSize);
        
        double sum=0;
        for(int i=0;i<m1.rowSize;i++){
            for(int j=0;j<m2.colSize;j++){
                for(int k=0;k<m1.colSize;k++){
                    result.matrix[i][j]+=m1.get(i, k)*m2.get(k, j);
                }
            }
        }
        
        return result;
    }
    
    public double sigmoid(double val){
        return (1/(1 + Math.exp(-val)));
    }

    public int getInputLayerSize() {
        return inputLayerSize;
    }

    public void setInputLayerSize(int inputLayerSize) {
        this.inputLayerSize = inputLayerSize;
    }

    public int getOutputLayerSize() {
        return outputLayerSize;
    }

    public void setOutputLayerSize(int outputLayerSize) {
        this.outputLayerSize = outputLayerSize;
    }

    public int getHiddenLayerSize() {
        return hiddenLayerSize;
    }

    public void setHiddenLayerSize(int hiddenLayerSize) {
        this.hiddenLayerSize = hiddenLayerSize;
    }

    public Matrix getHL2Matrix() {
        return HL2Matrix;
    }

    public void setHL2Matrix(Matrix HL2Matrix) {
        this.HL2Matrix = HL2Matrix;
    }

    public Matrix getFFWItoHMatrix() {
        return FFWItoHMatrix;
    }

    public void setFFWItoHMatrix(Matrix FFWItoHMatrix) {
        this.FFWItoHMatrix = FFWItoHMatrix;
    }

    public Matrix getFFWHtoIMatrix() {
        return FFWHtoIMatrix;
    }

    public void setFFWHtoIMatrix(Matrix FFWHtoIMatrix) {
        this.FFWHtoIMatrix = FFWHtoIMatrix;
    }

    public Matrix getFBWItoHMatrix() {
        return FBWItoHMatrix;
    }

    public void setFBWItoHMatrix(Matrix FBWItoHMatrix) {
        this.FBWItoHMatrix = FBWItoHMatrix;
    }

    public Matrix getFBWHtoIMatrix() {
        return FBWHtoIMatrix;
    }

    public void setFBWHtoIMatrix(Matrix FBWHtoIMatrix) {
        this.FBWHtoIMatrix = FBWHtoIMatrix;
    }

    public Matrix getFFBias() {
        return FFBias;
    }

    public void setFFBias(Matrix FFBias) {
        this.FFBias = FFBias;
    }

    public double getFFBiasOutput() {
        return FFBiasOutput;
    }

    public void setFFBiasOutput(double FFBiasOutput) {
        this.FFBiasOutput = FFBiasOutput;
    }

    public Matrix getFBBias() {
        return FBBias;
    }

    public void setFBBias(Matrix FBBias) {
        this.FBBias = FBBias;
    }

    public double getFBBiasOutput() {
        return FBBiasOutput;
    }

    public void setFBBiasOutput(double FBBiasOutput) {
        this.FBBiasOutput = FBBiasOutput;
    }

    public double getOutput() {
        return output;
    }

    public void setOutput(double output) {
        this.output = output;
    }

    public Matrix getRawHL2Matrix() {
        return rawHL2Matrix;
    }

    public void setRawHL2Matrix(Matrix rawHL2Matrix) {
        this.rawHL2Matrix = rawHL2Matrix;
    }
}
