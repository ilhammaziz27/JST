/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jstoccupation;

/**
 *
 * @author ilhammaziz
 */
public class Matrix {
    public int rowSize;
    public int colSize;
    public double[][] matrix;
    
    public Matrix(int rows,int cols){
        this.rowSize=rows;
        this.colSize=cols;
        matrix=new double[rows][cols];
        clear();
    }
    
    public Matrix(double[][] matrixData){
        this.rowSize=matrixData.length;
        this.colSize=matrixData[0].length;
        
        this.matrix=matrixData;
    }
    
    public Matrix(){
        
    }
    
    public Matrix createRowMatrix(double[] matrixData){
        Matrix newMatrix=new Matrix();
        newMatrix.rowSize=1;
        newMatrix.colSize=matrixData.length;
        newMatrix.matrix[1]=matrixData;
        
        return newMatrix;
    }
    
    public void printMatrix(){
        for(int i=0;i<rowSize;i++){
            for(int j=0;j<colSize;j++){
                System.out.printf(matrix[i][j]+"\t");
            }
            System.out.println("");
        }
    }
    
    public double get(int row,int col){
        return this.matrix[row][col];
    }
    
    public Matrix getRows(int rows){
        Matrix newMatrix=new Matrix(1, colSize);
        
        for(int i=0;i<colSize;i++){
            newMatrix.set(matrix[rows][i], 0, i);
        }
        
        return newMatrix;
    }
    
    public void clear(){
        for(int i=0;i<this.rowSize;i++){
            for(int j=0;j<this.colSize;j++){
                this.matrix[i][j]=0;
            }
        }
    }
    
    public void add(double[] dataRow){
        matrix[this.rowSize]=dataRow;
        this.rowSize++;
    }
    
    public double[][] cloneMatrix(){
        return this.matrix;
    }
    
    public boolean equals(Matrix m2){
        return (this.rowSize==m2.rowSize && this.colSize==m2.colSize);
    }
    
    public boolean isVector(){
        return false;
    }
    
    public boolean isZero(){
        return false;
    }
    
    public void set(double val,int row,int col){
        this.matrix[row][col]=val;
    }
}
