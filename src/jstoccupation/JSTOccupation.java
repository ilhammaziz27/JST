/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jstoccupation;

import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.Random;
import static jstoccupation.NeuralNetwork.randomize;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;

/**
 *
 * @author ilhammaziz
 */
public class JSTOccupation {
    private Matrix inputSets;
    private Matrix outputSets;
    private String datasetUrl;
    private int inputLayerSize;
    private int hiddenLayerSize;
    
    public JSTOccupation(){
        
    }
    
    public static void main(String[] args) {
        int il=4;
        int hl=4;
        int ol=1;
        JSTOccupation app=new JSTOccupation();
        
        /*Matrix FFWItoHMatrix=new Matrix(il, hl);
        FFWItoHMatrix.set(0.49, 0, 0);
        FFWItoHMatrix.set(0.63, 0, 1);
        FFWItoHMatrix.set(-0.88, 1, 0);
        FFWItoHMatrix.set(0.62, 1, 1);
        Matrix FFWHtoIMatrix=new Matrix(hl, 1);
        FFWHtoIMatrix.set(0.96,0,0);
        FFWHtoIMatrix.set(0.24,1,0);*/
        
        Matrix FFWItoHMatrix=app.generateMatrix(il, hl);
        Matrix FFWHtoIMatrix=app.generateMatrix(hl,1);        
        
        //Matrix FFBias=app.generateMatrix(1,hl);
        Matrix FBWItoHMatrix=new Matrix(il, hl);
        Matrix FBWHtoIMatrix=new Matrix(hl, 1);
        //Matrix FBBias=new Matrix(1, hl);
        //double FFBiasOutput=randomize(-1, 1);
        
        //FBBias.clear();
        
        String url="//home//ilhammaziz//NetBeansProjects//JSTOccupation//dataset//occupancy_normal.xlsx";
        app.setDatasetUrl(url);
        app.loadInputSet(il);
        app.loadOutputSet(ol);
        
        int i=0;
        int epoch=4;
        int itEpoch=0;
        double sumErr=0;
        int TRUE=0;
        int FALSE=0;
        
        NeuralNetwork nn=new NeuralNetwork(il,hl,1);
        
        DecimalFormat df = new DecimalFormat("#.###"); 
        
//        Matrix m1=app.generateMatrix(3, 2);
//        Matrix m2=app.generateMatrix(2, 3);
//        Matrix m3=nn.dotProduct(m1, m2);
//        m1.printMatrix();
//        System.out.println("");
//        m2.printMatrix();
//        System.out.println("");
//        m3.printMatrix();
        //System.out.printf("No\tI1\tI2\tw11\tw21\tw12\tw22\tu1\tu2\th1\th2\twi1\twi2\tYRaw\tYHat\tO\tErr\tdw11\tdw12\tdw21\tdw22\n");
        while(i<app.inputSets.rowSize){
            if(i%epoch==0){
                System.out.printf("Epoch : "+i/4+"\t -> MSE : "+(df.format(sumErr/epoch))+"\n");
                sumErr=0;
            }
            
            if(i!=0){
                FFWHtoIMatrix=app.addition(FFWHtoIMatrix, nn.getFBWHtoIMatrix());
                FFWItoHMatrix=app.addition(FFWItoHMatrix, nn.getFBWItoHMatrix());
                //FFBias=app.addition(FFBias, nn.getFBBias());
                //FFBiasOutput+=nn.getFBBiasOutput();
            }
            
            nn=new NeuralNetwork(il,hl,1);
            nn.setFFWItoHMatrix(FFWItoHMatrix);
            nn.setFFWHtoIMatrix(FFWHtoIMatrix);
            //nn.setFFBias(FFBias);
            //nn.setFFBiasOutput(FFBiasOutput);
            //nn.setFBBias(FBBias);
            nn.setFBWItoHMatrix(FBWItoHMatrix);
            nn.setFBWHtoIMatrix(FBWHtoIMatrix);
            
            double yRaw=nn.feedForward(app.inputSets.getRows(i));
            double yHat=nn.sigmoid(yRaw);
            //System.out.println(Math.round(yHat));
            
            if(app.outputSets.get(i, 0)==Math.round(yHat))
                TRUE++;
            else
                FALSE++;
            
            double err=app.outputSets.get(i,0)-yHat;
            sumErr+=Math.pow(err,2);
            nn.feedBack(app.inputSets.getRows(i), err, 0.5);
            
            
            //--------------------OUTPUTKAN-------------------------
//            System.out.printf(i+"\t");
//            for(int n=0;n<app.inputSets.colSize;n++)
//                System.out.printf(df.format(app.inputSets.get(i,n))+"\t");
//            
//            for(int w1=0;w1<il;w1++)
//                for(int w2=0;w2<hl;w2++)
//                    System.out.printf(df.format(nn.getFFWItoHMatrix().get(w1,w2))+"\t");
//
//            for(int h=0;h<hl;h++)
//                System.out.printf(df.format(nn.getRawHL2Matrix().get(0,h))+"\t");
//            
//            for(int h=0;h<hl;h++)
//                System.out.printf(df.format(nn.getHL2Matrix().get(0,h))+"\t");
//            
//            for(int w1=0;w1<hl;w1++)
//                    System.out.printf(df.format(nn.getFFWHtoIMatrix().get(w1,0))+"\t");
//            
//            System.out.printf("|"+df.format(yRaw)+"\t"+df.format(yHat)+"\t"+df.format(app.outputSets.get(i, 0))+"\t"+df.format(err)+"\t");
//            
//            for(int m=0;m<il;m++)
//                for(int n=0;n<hl;n++)
//                    System.out.printf(df.format(nn.getFBWItoHMatrix().get(m,n))+"\t");
//            
//            for(int m=0;m<hl;m++)
//                    System.out.printf(df.format(nn.getFBWHtoIMatrix().get(m,0))+"\t");
//            
//            System.out.println("");
//            //============END OF OUTPUT=================
            
            i++;
        }
        System.out.println("Total True : "+TRUE);
        System.out.println("Total False : "+FALSE);
        double dTrue=(double) TRUE;
        double accur=dTrue/app.inputSets.rowSize*100;
        System.out.println("Accuracy : "+ (df.format(accur))+"%");
    }
    
    public Matrix addition(Matrix m1,Matrix m2){
        //System.out.println(m1.rowSize+"x"+m1.colSize);
        //System.out.println(m2.rowSize+"x"+m2.colSize);
        Matrix newMatrix=new Matrix(m1.rowSize, m1.colSize);
        for(int i=0;i<m1.rowSize;i++){
            for(int j=0;j<m1.colSize;j++){
                newMatrix.set(m1.get(i, j)+m2.get(i, j), i, j);
            }
        }
        
        return newMatrix;
    }
    
    public void loadOutputSet(int ol){
        boolean firstRow=true;
        
        XSSFSheet sheet=this.importDataset();
        Iterator<Row> rows = sheet.iterator();
        
        int i=0;
        
        outputSets=new Matrix(sheet.getPhysicalNumberOfRows()-1, ol);
        
        while(rows.hasNext()){
            Row row = rows.next();
            
            if(firstRow){
                firstRow=false;
                continue;
            }
            
            outputSets.set(Double.parseDouble(String.valueOf(row.getCell(4).getNumericCellValue())), i, 0);
            i++;
        }
    }
    
    public void loadInputSet(int il){
        boolean firstRow=true;
        
        XSSFSheet sheet=this.importDataset();
        Iterator<Row> rows = sheet.iterator();
        
        int i=0;
        
        inputSets=new Matrix(sheet.getPhysicalNumberOfRows()-1, il);
        
        while(rows.hasNext()){
            Row row = rows.next();
            
            if(firstRow){
                firstRow=false;
                continue;
            }
            
            for(int j=0;j<il;j++){
                inputSets.set(Double.parseDouble(String.valueOf(row.getCell(j).getNumericCellValue())), i, j);
            }
            
            i++;
        }
    }
    
    public XSSFSheet importDataset(){
        ExcelUtil eu=new ExcelUtil(this.datasetUrl);
        XSSFSheet sheet=eu.getExcelSheet();        
        
        return sheet;
    }

    public Matrix generateMatrix(int rows,int cols){
        Matrix newMatrix=new Matrix(rows, cols);
        for(int i=0;i<rows;i++){
            for(int j=0;j<cols;j++){
                newMatrix.set(randomize(-1, 1), i, j);
            }
        }
        
        return newMatrix;
    }
    
    public String getDatasetUrl() {
        return datasetUrl;
    }

    public void setDatasetUrl(String datasetUrl) {
        this.datasetUrl = datasetUrl;
    }
}
